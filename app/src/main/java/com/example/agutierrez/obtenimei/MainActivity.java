package com.example.agutierrez.obtenimei;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        TextView textDeviceID = (TextView)findViewById(R.id.deviceId);

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        textDeviceID.setText(getDeviceID(tm));
    }

    private String getDeviceID(TelephonyManager pm){

        String id = pm.getDeviceId();

        if (id==null){
            id = "no esta disponible";
        }

        int pt = pm.getPhoneType();
        switch (pt){

            case TelephonyManager.PHONE_TYPE_NONE:
                return "Ninguno : IMEI = "+id;
            case TelephonyManager.PHONE_TYPE_GSM:
                return  "GSM : IMEI = " + id;

            case TelephonyManager.PHONE_TYPE_CDMA:
                return  "CDMA : MEID/ESN = " + id;
            default:
                return "Desconocido DELIUS IMEI = " + id;
        }
    }

}
